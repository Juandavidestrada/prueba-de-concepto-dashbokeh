from django.contrib import admin
from django.urls import path
from django.conf.urls import include
urlpatterns = [
    path('admin/', admin.site.urls),
    path('plotly', include('django_dash.urls')),
    path('bokeh', include('bokeh_app.urls'))
]
