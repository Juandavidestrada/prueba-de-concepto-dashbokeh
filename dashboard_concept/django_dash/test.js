const {cfEnabled, cfLayoutOverrides, cfPlotSetup} = window['crosslink-plotly'].js
const crossfiltering = true
const plots = all_sets
const renderOneDiv = idArray => {
  const root = idArray.length ? document.getElementById(`gd_${idArray.slice(0, -1).join('_')}`) : document.body
  const div = document.createElement('div')
  div.setAttribute('id', `gd_${idArray.join('_')}`)
  if(idArray.length) {
    div.setAttribute('class', 'plot-container')
  }
  root.appendChild(div)
  return div
}

const histogram = true

renderOneDiv([])

const renderedDivs = []
const offset = 1
plots.slice(offset, 31).forEach((dataset, i) => {
  const plotEl = renderOneDiv([`wbcd_${i}`], true)
  const data ='this is data'
  const plotContent = {data, layout}
  const crossfilterEnabled = cfEnabled(crossfiltering, plotContent);
  const plot = Plotly.plot(plotEl, data, layout);
  if (crossfilterEnabled) {
    cfPlotSetup(Plotly, renderedDivs, plot, plotEl, plotContent);
  }
  renderedDivs.push(plotEl)
})
