from django.shortcuts import render
from django.views.generic import TemplateView
from . import plots

class DashboardView(TemplateView):
    template_name = 'plot.html'

    def get_context_data(self, **kwargs):
        context = super(DashboardView, self).get_context_data(**kwargs)
        context['3dplot'] = plots.get_topographical_3D_surface_plot()
        return context
