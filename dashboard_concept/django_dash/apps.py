from django.apps import AppConfig


class DjangoDashConfig(AppConfig):
    name = 'django_dash'
