from plotly.offline import plot
import plotly.graph_objs as go
import pandas as pd
from datetime import datetime
from plotly.subplots import make_subplots

import requests
def get_topographical_3D_surface_plot():
    raw_data = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/api_docs/mt_bruno_elevation.csv')
    layout = go.Layout(
        autosize=False,
        width=1200,
        height=700,
        margin=dict(
            l=65,
            r=50,
            b=65,
            t=90
            )
        )
    fig = make_subplots(rows=1, cols=2)
    fig.add_trace(go.Bar(y=[2, 1, 3]), row=1, col=1)
    fig.add_trace(go.Bar(y=[2, 1, 3]), row=1, col=2)
    plot_div = plot(fig, output_type='div',filename='elevations-3d-surface')
    return plot_div
