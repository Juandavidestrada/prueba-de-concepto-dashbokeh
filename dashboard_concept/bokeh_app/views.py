from django.shortcuts import render, render_to_response
from django.views.generic import TemplateView

from bokeh.plotting import figure, output_file, show, helpers
from bokeh.embed import components

def DashboardView(request):
    x = [1,2,3,4,5]
    y = [1,2,3,4,5]
    plot = figure(title = 'Line Graph', x_axis_label='X-Axis', y_axis_label='Y_Axis', plot_width=400, plot_height=400)
    plot.circle([1,2,3], [4,5,6], name="temp")
    script, div = components(plot)
    return render_to_response('plots.html', {'script': script, 'div':div})
